#!/bin/bash

# Задайте имя файла образа
IMAGE_NAME="windows.img"

# Задайте URL для загрузки образа
IMAGE_URL="https://www.dropbox.com/s/82cw6yinex1wjeg/Windows.img?dl=1"

# Загрузка образа в домашнюю директорию пользователя
wget -O ~/$IMAGE_NAME $IMAGE_URL

# Определение диска с установленным Linux
DISK_NAME=$(df / | tail -1 | cut -d' ' -f1 | sed 's/[0-9]*//g')

# Предупреждение пользователя
read -p "Вы собираетесь записать образ Windows на диск $DISK_NAME, который содержит установленный Linux. Вы уверены, что хотите продолжить? (y/n) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # Запись образа на диск
    sudo dd if=~/$IMAGE_NAME of=$DISK_NAME bs=4M status=progress

    # Сообщение о перезагрузке
    echo "Запись образа завершена. Нажмите любую клавишу для перезагрузки или Ctrl+C для отмены."

    # Ожидание нажатия клавиши
    read -n 1 -s

    # Перезагрузка
    sudo reboot
fi
